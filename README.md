# Flower Spot
>  App used for flower spotting


## Table of contents
* [General info](#general-info)
* [Setup](#setup)
* [Check Swagger](#Check-Swagger)
* [Screenshots](#screenshots)
* [Features](#features)
* [Contact](#contact)

## General info
The app is used for flower spotting while hiking, traveling, etc. Users can check out different flowers, their details, and sightings as well as add their sightings. Think of it as Instagram, but only for flowers, with a list of flowers as well.

## Setup
1. Set up application.properties as shown bellow for communication with database.
	Database used is mySql. 

	Important:* Please remove quotation marks in fields you should fill out 
```java
	spring.datasource.url = jdbc:mysql://localhost:3306/"yourDatabaseName"?useSSL=false
	spring.datasource.username = "your mysql database username"
	spring.datasource.password = "your password of mysql database"
	spring.jpa.show-sql = true
	spring.jpa.hibernate.ddl-auto = update
```
---
2. Change path of folder of pictures inside FlowerSpotApplication.class into your path of the same folder
	
  `String folderPath = "/home/benjamin/Downloads/flowerSpot/src/main/resources/images";`
  

3. After setting up Database run application so that tables are created inside of DataBase.


4. Happy using the app

## Check Swagger
**Important**
Api Documentation not fully completed!!!

To access swagger documentation type: 
`http://localhost:8080/swagger-ui.html/`

Here you can check all Api Documentation.

**Important**
Because there are API's for which you need to be autorized to access you have to get JWT token and inject it into
Swagger.

1. Make a registration API execution with requested body 

2. Execute authentication APIi with username and password you provided in registration section

3. JWT token will be returned from authentication API copy and paste it into Authorization of Swagger



## Features
List of features
* Registration of user
* Authentication of user
* Creating the sightings
* Deleting the sightings 
* Checking the list of sightings
* Checking the list of flowers
* Like the sighting
* Delete Like from particular sighting


## Contact
Created by [@benjamin](benjamin.dedic@scaleup.ba) - feel free to contact me!
