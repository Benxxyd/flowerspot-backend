package com.assignment.flowerSpot;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.assignment.flowerSpot.model.Flower;
import com.assignment.flowerSpot.repository.IFlower;

@SpringBootApplication
public class FlowerSpotApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlowerSpotApplication.class, args);
	}

	@Bean
	CommandLineRunner init(IFlower repo) {
		return args -> {
			appReady(repo);
		};
	}

	public void appReady(IFlower flowerRepository) {
		String folderPath = "/home/benjamin/Downloads/flowerSpot/src/main/resources/images";
		File folder = new File(folderPath);

		File[] listOfFiles = folder.listFiles();

		List<Flower> list = new ArrayList<Flower>();

		for (File file : listOfFiles) {
			if (file.isFile()) {

				System.out.println(file.getName());
				System.out.println(file.getAbsolutePath());

				Flower flower = new Flower();

				flower.setName(file.getName());
				flower.setDescription(file.getName());
				flower.setImageURL(file.getAbsolutePath());
				list.add(flower);

			}

		}

		for (int i = 0; i < 10; i++) {
			Random random = new Random();
			random.setSeed(0);
			int index = random.nextInt(list.size());
			flowerRepository.save(list.get(index));
			list.remove(index);
		}

	}

}
