package com.assignment.flowerSpot.database;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.assignment.flowerSpot.exceptions.EntityNotFoundException;
import com.assignment.flowerSpot.model.Flower;
import com.assignment.flowerSpot.repository.IFlower;

@Component
public class FlowerDatabaseService {

  @Autowired
  IFlower flower;

  public List<Flower> getAllFlowers() {

    List<Flower> listOfFlowers = flower.findAll();
    if (listOfFlowers.isEmpty()) {
      throw new EntityNotFoundException("There are no flowers");
    }
    return listOfFlowers;
  }

  public Flower getFlower(Long flowerId) {
    Flower flowerById = flower.getOne(flowerId);
    if (flowerById == null) {
      throw new EntityNotFoundException("There is no flower with id: " + flowerId);
    }
    return flowerById;
  }

}
