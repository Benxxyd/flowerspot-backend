package com.assignment.flowerSpot.database;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.assignment.flowerSpot.exceptions.BadRequestException;
import com.assignment.flowerSpot.model.Sighting;
import com.assignment.flowerSpot.repository.ISighting;

@Component
public class SightingDatabaseService {

  @Autowired
  ISighting sighting;

  public List<Sighting> getAllSightings() {
    return sighting.findAll();
  }

  public List<Sighting> getAllByFlowerId(Long flowerId) {
    return sighting.findAllByFlowerId(flowerId);
  }

  public Sighting getSightingById(Long id) {
    return sighting.findById(id).orElseThrow(
        () -> new BadRequestException("Sighting with id: " + id + " could not be found"));
  }

}
