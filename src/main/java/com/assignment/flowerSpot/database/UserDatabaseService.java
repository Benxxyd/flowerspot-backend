package com.assignment.flowerSpot.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.assignment.flowerSpot.model.User;
import com.assignment.flowerSpot.repository.IUser;

@Component
public class UserDatabaseService {

	@Autowired
	IUser user;

	public User getUserByUsername(String username) {
		return user.findByUsername(username);

	}

	public User createNewUser(User newUser) {
		return user.save(newUser);
	}

	public User getOne(Long userId) {
		return user.getOne(userId);

	}

}
