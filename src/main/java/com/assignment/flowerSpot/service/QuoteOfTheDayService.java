package com.assignment.flowerSpot.service;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.assignment.flowerSpot.model.QuoteOfTheDayAPI;

import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

@Service
public class QuoteOfTheDayService {

	QuoteOfTheDayAPI service;

	public String getQuoteOfTheDay() throws IOException {

		Retrofit retrofit = new Retrofit.Builder().baseUrl("https://quotes.rest")
				.addConverterFactory(GsonConverterFactory.create()).build();

		service = retrofit.create(QuoteOfTheDayAPI.class);

		return service.getQuotes().execute().body().getContents().getQuotes().get(0).getQuote();

	}

}
