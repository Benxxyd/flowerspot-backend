package com.assignment.flowerSpot.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.assignment.flowerSpot.config.JwtTokenUtil;
import com.assignment.flowerSpot.repository.IUser;

@Service
public class JwtUserDetailsService implements UserDetailsService {

	@Autowired
	private IUser userRepository;

	@Autowired
	JwtTokenUtil jwtTokenUtil;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		com.assignment.flowerSpot.model.User user = userRepository.findByUsername(username);

		if (user.getUsername().equals(username)) {
			return new User(user.getUsername(), user.getPassword(), new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}

	public String returnUserFromJwtToken(String requestTokenHeader) {

		String username = null;
		String jwtToken = null;

		jwtToken = requestTokenHeader.substring(7);

		username = jwtTokenUtil.getUsernameFromToken(jwtToken);

		if (username != null) {
			return username;
		} else {
			System.out.println("NO USERNAME");
			return "Username does not exists";
		}

	}
}