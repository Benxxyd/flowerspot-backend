package com.assignment.flowerSpot.service;

import java.io.IOException;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.assignment.flowerSpot.config.JwtTokenUtil;
import com.assignment.flowerSpot.database.FlowerDatabaseService;
import com.assignment.flowerSpot.database.SightingDatabaseService;
import com.assignment.flowerSpot.database.UserDatabaseService;
import com.assignment.flowerSpot.exceptions.SightingException;
import com.assignment.flowerSpot.model.Sighting;
import com.assignment.flowerSpot.model.SightingRequest;
import com.assignment.flowerSpot.model.User;
import com.assignment.flowerSpot.repository.ISighting;

@Service
public class SightingService {

  @Autowired
  ISighting sightingRepository;

  @Autowired
  FlowerDatabaseService flowerDatabase;

  @Autowired
  QuoteOfTheDayService quoteOfTheDayService;

  @Autowired
  JwtTokenUtil jwtTokenUtil;

  @Autowired
  JwtUserDetailsService jwtUserDetailsService;

  @Autowired
  SightingDatabaseService sightingDatabaseService;

  @Autowired
  UserDatabaseService userDatabase;

  public List<Sighting> getAllSightings() {
    return sightingDatabaseService.getAllSightings();
  }

  public List<Sighting> getAllByFlowerId(Long flowerId) {

    List<Sighting> allSightings = sightingDatabaseService.getAllByFlowerId(flowerId);
    if (allSightings.isEmpty()) {
      throw new EntityNotFoundException("There are no any sightings");
    }
    return allSightings;
  }

  public Sighting createNewSighting(SightingRequest sightingRequest) throws IOException {

    Sighting sighting = new Sighting();
    sighting.setLat(sightingRequest.getLat());
    sighting.setLon(sightingRequest.getLon());
    sighting.setImage(sightingRequest.getImage());
    sighting.setFlower(flowerDatabase.getFlower(sightingRequest.getFlowerId()));
    sighting.setUser(userDatabase.getOne(sightingRequest.getUserId()));
    // sighting.setQuote(quoteOfTheDayService.getQuoteOfTheDay());
    return sightingRepository.save(sighting);

  }

  public void deleteSighting(HttpServletRequest request, Long id) throws SightingException {

    final String requestTokenHeader = request.getHeader("Authorization");

    String username = null;
    String jwtToken = null;

    jwtToken = requestTokenHeader.substring(7);

    username = jwtTokenUtil.getUsernameFromToken(jwtToken);

    User user = userDatabase.getUserByUsername(username);
    Sighting sighting = sightingRepository.getOne(id);

    if (sighting == null) {
      throw new SightingException("Sighting with id: " + id + " does not exists");
    } else {
      Long sightiningUserId = sighting.getUser().getId();

      if (user.getId() == sightiningUserId) {
        sightingRepository.delete(sighting);

      } else {
        throw new SightingException("You can not delete sighting which you did not created");
      }
    }



  }

}
