package com.assignment.flowerSpot.service;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.assignment.flowerSpot.config.JwtTokenUtil;
import com.assignment.flowerSpot.database.SightingDatabaseService;
import com.assignment.flowerSpot.database.UserDatabaseService;
import com.assignment.flowerSpot.exceptions.BadRequestException;
import com.assignment.flowerSpot.model.Sighting;
import com.assignment.flowerSpot.model.User;

@Service
public class LikeService {

  @Autowired
  JwtTokenUtil jwtTokenUtil;

  @Autowired
  UserDatabaseService userDatabaseService;

  @Autowired
  SightingDatabaseService sightingDatabaseService;

  @Autowired
  JwtUserDetailsService jwtUserDetailsService;

  String username = null;
  String jwtToken = null;

  public String likeSight(HttpServletRequest request, Long id) throws BadRequestException {

    String username = getUsernameFromRequest(request);

    User user = userDatabaseService.getUserByUsername(username);
    Sighting sighting = sightingDatabaseService.getSightingById(id);

    User userWhoLikedSighting =
        sighting.getUsers().stream().filter(s -> user.equals(s)).findAny().orElse(null);

    if (userWhoLikedSighting != null)
      throw new BadRequestException("Already liked sight!");

    {
      sighting.getUsers().add(user);
      user.getSighting().add(sighting);

      userDatabaseService.createNewUser(user);
      return "Successfully liked sighting";
    }
  }

  public String deleteLikeForParticularSight(HttpServletRequest request, Long sightId) {

    String username = getUsernameFromRequest(request);
    System.out.println(username);

    User user = userDatabaseService.getUserByUsername(username);
    Sighting sighting = sightingDatabaseService.getSightingById(sightId);

    User userWhoLikedSighting =
        sighting.getUsers().stream().filter(s -> user.equals(s)).findAny().orElse(null);

    if (userWhoLikedSighting == null)
      throw new BadRequestException("Can not delete like you did not create!");

    {
      sighting.getUsers().remove(user);
      user.getSighting().remove(sighting);
      userDatabaseService.createNewUser(user);
      return "Successfully deleted sighting with id: " + sightId;
    }
  }

  private String getUsernameFromRequest(HttpServletRequest request) {

    final String requestTokenHeader = request.getHeader("Authorization");
    jwtToken = requestTokenHeader.substring(7);

    return jwtTokenUtil.getUsernameFromToken(jwtToken);
  }

}
