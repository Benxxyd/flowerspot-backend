package com.assignment.flowerSpot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.assignment.flowerSpot.database.UserDatabaseService;
import com.assignment.flowerSpot.exceptions.BadRequestException;
import com.assignment.flowerSpot.model.User;

@Service
public class RegisterationService {

  @Autowired
  UserDatabaseService userDatabase;

  public User userRegistration(User user) throws BadRequestException {

    User newUser = new User();

    if (userDatabase.getUserByUsername(user.getUsername()) != null)
      throw new BadRequestException("User already exists!!!");
    {
      newUser.setEmail(user.getEmail());
      newUser.setUsername(user.getUsername());

      BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

      newUser.setPassword(passwordEncoder.encode(user.getPassword()));

      userDatabase.createNewUser(newUser);
      return newUser;
    }

  }

}
