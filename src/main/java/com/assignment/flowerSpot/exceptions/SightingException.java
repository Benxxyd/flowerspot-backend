package com.assignment.flowerSpot.exceptions;

public class SightingException extends RuntimeException {

  public SightingException(String message) {
    super(message);
  }

  public SightingException(String message, Throwable cause) {
    super(message, cause);
  }

}
