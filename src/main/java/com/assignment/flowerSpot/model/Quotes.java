package com.assignment.flowerSpot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Quotes {

	@SerializedName("success")
	@Expose
	private QuoteSuccess success;
	@SerializedName("contents")
	@Expose
	private QuoteContents contents;

	public QuoteSuccess getSuccess() {
		return success;
	}

	public void setSuccess(QuoteSuccess success) {
		this.success = success;
	}

	public QuoteContents getContents() {
		return contents;
	}

	public void setContents(QuoteContents contents) {
		this.contents = contents;
	}

}