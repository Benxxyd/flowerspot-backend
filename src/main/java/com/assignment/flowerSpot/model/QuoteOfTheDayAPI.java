package com.assignment.flowerSpot.model;

import retrofit2.Call;
import retrofit2.http.GET;

public interface QuoteOfTheDayAPI {

	@GET("/qod?category=inspire")
	Call<Quotes> getQuotes();
}
