package com.assignment.flowerSpot.model;

public class SightingRequest {

	private Double lat;
	private Double lon;

	private Long userId;
	private Long flowerId;

	private String image;

	private String quote;

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getFlowerId() {
		return flowerId;
	}

	public void setFlowerId(Long flowerId) {
		this.flowerId = flowerId;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getQuote() {
		return quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}

}
