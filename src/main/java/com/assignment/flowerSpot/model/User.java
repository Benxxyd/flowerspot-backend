package com.assignment.flowerSpot.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class User {

	@Id
	@GeneratedValue
	private Long id;

	private String email;
	private String username;
	private String password;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "SightingUserLike", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "sighting_id", referencedColumnName = "id"))
	private Set<Sighting> sighting = new HashSet<>();

	public User() {
		// TODO Auto-generated constructor stub
	}

	public User(String username, String password, String email) {
		this.username = username;
		this.password = password;
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", email=" + email + ", username=" + username + ", password=" + password
				+ ", sighting=" + sighting + "]";
	}

	public Set<Sighting> getSighting() {
		return sighting;
	}

	public void setSighting(Set<Sighting> sighting) {
		this.sighting = sighting;
	}

}
