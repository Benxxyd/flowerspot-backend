package com.assignment.flowerSpot.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Quote {

	@SerializedName("quote")
	@Expose
	private String quote;
	@SerializedName("author")
	@Expose
	private String author;
	@SerializedName("length")
	@Expose
	private String length;
	@SerializedName("tags")
	@Expose
	private List<String> tags = null;
	@SerializedName("category")
	@Expose
	private String category;
	@SerializedName("title")
	@Expose
	private String title;
	@SerializedName("date")
	@Expose
	private String date;
	@SerializedName("id")
	@Expose
	private Object id;

	public String getQuote() {
		return quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Object getId() {
		return id;
	}

	public void setId(Object id) {
		this.id = id;
	}

}
