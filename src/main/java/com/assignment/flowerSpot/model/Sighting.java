package com.assignment.flowerSpot.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

@Entity

public class Sighting {

  @Id
  @GeneratedValue
  private Long id;

  private Double lat;
  private Double lon;

  @OneToOne
  private User user;
  @OneToOne
  private Flower flower;

  private String image;

  private String quote;

  @ManyToMany(mappedBy = "sighting")
  private Set<User> users = new HashSet<>();

  public Sighting(Long id, Double lat, Double lon, User user, Flower flower, String image,
      String quote, Set<User> users) {
    super();
    this.id = id;
    this.lat = lat;
    this.lon = lon;
    this.user = user;
    this.flower = flower;
    this.image = image;
    this.quote = quote;
    this.users = users;
  }

  public Sighting(Double lat, Double lon, User user, Flower flower, String image, String quote) {
    super();

    this.lat = lat;
    this.lon = lon;
    this.user = user;
    this.flower = flower;
    this.image = image;
    this.quote = quote;
  }


  public Sighting() {
    super();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Double getLat() {
    return lat;
  }

  public void setLat(Double lat) {
    this.lat = lat;
  }

  public Double getLon() {
    return lon;
  }

  public void setLon(Double lon) {
    this.lon = lon;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Flower getFlower() {
    return flower;
  }

  public void setFlower(Flower flower) {
    this.flower = flower;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public String getQuote() {
    return quote;
  }

  public void setQuote(String quote) {
    this.quote = quote;
  }

  @Override
  public String toString() {
    return "Sighting [id=" + id + ", lat=" + lat + ", lon=" + lon + ", user=" + user + ", flower="
        + flower + "sightings, image=" + image + ", quote=" + quote + ", users=" + users + "]";
  }

  public Set<User> getUsers() {
    return users;
  }

  public void setUsers(Set<User> users) {
    this.users = users;
  }

}
