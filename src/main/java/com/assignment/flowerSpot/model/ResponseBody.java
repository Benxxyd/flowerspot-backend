package com.assignment.flowerSpot.model;

public class ResponseBody {


  private String message;
  private Object result;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Object getResult() {
    return result;
  }

  public void setResult(Object result) {
    this.result = result;
  }



}
