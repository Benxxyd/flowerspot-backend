package com.assignment.flowerSpot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.assignment.flowerSpot.model.Flower;

@Repository
public interface IFlower extends JpaRepository<Flower, Long> {

  public Flower findByName(String name);

}
