package com.assignment.flowerSpot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.assignment.flowerSpot.model.User;

@Repository
public interface IUser extends JpaRepository<User, Long> {

  public User findByUsername(String username);

}
