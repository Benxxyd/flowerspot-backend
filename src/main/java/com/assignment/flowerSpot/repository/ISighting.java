package com.assignment.flowerSpot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assignment.flowerSpot.model.Sighting;

@Repository
public interface ISighting extends JpaRepository<Sighting, Long> {

	List<Sighting> findAllByFlowerId(Long flowerId);

	Sighting findByFlowerId(Long flowerId);

}