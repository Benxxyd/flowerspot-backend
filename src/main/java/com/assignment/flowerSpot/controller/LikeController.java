package com.assignment.flowerSpot.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.assignment.flowerSpot.exceptions.BadRequestException;
import com.assignment.flowerSpot.service.LikeService;

@RestController
public class LikeController {

  @Autowired
  LikeService likeService;

  @RequestMapping(method = RequestMethod.POST, value = "/likeThisSight/{id}")
  public ResponseEntity<String> likeSight(HttpServletRequest request,
      @PathVariable(value = "id") Long id) throws BadRequestException {

    return new ResponseEntity<String>(likeService.likeSight(request, id), HttpStatus.OK);
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/deleteLikeOnSight/{id}")
  public ResponseEntity<String> deleteLike(HttpServletRequest request,
      @PathVariable(value = "id") Long sightId) throws BadRequestException {

    return new ResponseEntity<String>(likeService.deleteLikeForParticularSight(request, sightId),
        HttpStatus.OK);
  }


}

