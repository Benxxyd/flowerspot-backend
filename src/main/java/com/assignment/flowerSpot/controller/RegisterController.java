package com.assignment.flowerSpot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.assignment.flowerSpot.model.ResponseBody;
import com.assignment.flowerSpot.model.User;
import com.assignment.flowerSpot.service.RegisterationService;

@RestController
public class RegisterController {

  @Autowired
  RegisterationService registrationService;

  @RequestMapping(value = "user/register", method = RequestMethod.POST,
      consumes = "application/json", produces = "application/json")
  public ResponseEntity<ResponseBody> register(@RequestBody User user) {

    ResponseBody apiResponse = new ResponseBody();

    User newUser = registrationService.userRegistration(user);

    apiResponse.setMessage("User Successfully created!");
    apiResponse.setResult(newUser);
    return new ResponseEntity<ResponseBody>(apiResponse, HttpStatus.OK);

  }
}


