package com.assignment.flowerSpot.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.assignment.flowerSpot.model.ResponseBody;
import com.assignment.flowerSpot.model.Sighting;
import com.assignment.flowerSpot.model.SightingRequest;
import com.assignment.flowerSpot.service.SightingService;

@RestController
public class SightingController {

  @Autowired
  SightingService sightingSerice;

  @RequestMapping(method = RequestMethod.GET, value = "/sighting")
  public ResponseEntity<List<Sighting>> allSightings() {

    List<Sighting> allSighting = sightingSerice.getAllSightings();
    return new ResponseEntity<List<Sighting>>(allSighting, HttpStatus.OK);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/sighting/byFlowerId/{id}")
  public Collection<Sighting> getAllSightingsForParticularFlower(
      @PathVariable(value = "id") Long flowerId) {
    return sightingSerice.getAllByFlowerId(flowerId);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/sighting/create")
  public ResponseEntity<ResponseBody> createSighting(@RequestBody SightingRequest sightingRequest)
      throws IOException {
    ResponseBody apiResponse = new ResponseBody();

    Sighting sighting = sightingSerice.createNewSighting(sightingRequest);

    apiResponse.setMessage("Successfully created sighting");
    apiResponse.setResult(sighting);

    return new ResponseEntity<ResponseBody>(apiResponse, HttpStatus.OK);

  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/sighting/{id}")
  public ResponseEntity<String> deleteSighting(HttpServletRequest request,
      @PathVariable(value = "id") Long id) {

    sightingSerice.deleteSighting(request, id);
    return new ResponseEntity<>("Successfully deleted sight!", HttpStatus.OK);
  }

}
