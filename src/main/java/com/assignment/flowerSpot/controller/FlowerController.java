package com.assignment.flowerSpot.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.assignment.flowerSpot.database.FlowerDatabaseService;
import com.assignment.flowerSpot.exceptions.EntityNotFoundException;
import com.assignment.flowerSpot.model.Flower;

@RestController
public class FlowerController {

  @Autowired
  FlowerDatabaseService flowerDatabase;

  @RequestMapping(value = "/flowers", method = RequestMethod.GET)
  ResponseEntity<List<Flower>> allFlowers() throws EntityNotFoundException {
    return ResponseEntity.status(HttpStatus.OK).body(flowerDatabase.getAllFlowers());
  }

}
