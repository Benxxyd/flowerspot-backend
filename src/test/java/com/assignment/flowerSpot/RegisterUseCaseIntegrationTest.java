package com.assignment.flowerSpot;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.assignment.flowerSpot.model.User;
import com.assignment.flowerSpot.repository.IUser;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "/application-test.properties")
public class RegisterUseCaseIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	IUser userRepository;

	@Test
	void registrationWorksThroughAllLayers() throws Exception {
		User user = new User("Admin", "Admin", "Admin@gmail.com");

		mockMvc.perform(
				post("/register").contentType("application/json").param("username", "Admin").param("password", "Admin")
						.param("email", "Admin@gmail,com").content(objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk());

		User userInDB = userRepository.findByUsername("Admin");
		String email = userInDB.getEmail();

		assertEquals(email, user.getEmail());
	}
}