package com.assignment.flowerSpot;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.assignment.flowerSpot.config.JwtTokenUtil;
import com.assignment.flowerSpot.model.User;
import com.assignment.flowerSpot.repository.IUser;
import com.assignment.flowerSpot.service.JwtUserDetailsService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "/application-test.properties")
public class JwtAuthenticationControllerTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@Autowired
	private IUser userRepository;

	@Test
	public void shouldNotAllowAccessToUnauthenticatedUsers() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/createSighting")).andExpect(status().isUnauthorized());
	}

	@Test
	public void shoudlAllowAccessToUnathenticatedUser() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/flowers")).andExpect(status().isOk());
	}

	@Test
	public void shouldGenerateAuthToken() throws Exception {

		User user = new User("admin", "admin", "admin@gmail.com");
		userRepository.save(user);

		final UserDetails userDetails = userDetailsService.loadUserByUsername("admin");
		final String token = jwtTokenUtil.generateToken(userDetails);

		assertNotNull(token);
		mvc.perform(MockMvcRequestBuilders.get("/sighting").header("Authorization", "Bearer " + token))
				.andExpect(status().isOk());
	}

}