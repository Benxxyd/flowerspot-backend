package com.assignment.flowerSpot;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.assignment.flowerSpot.config.JwtTokenUtil;
import com.assignment.flowerSpot.model.Sighting;
import com.assignment.flowerSpot.model.User;
import com.assignment.flowerSpot.repository.IFlower;
import com.assignment.flowerSpot.repository.ISighting;
import com.assignment.flowerSpot.repository.IUser;
import com.assignment.flowerSpot.service.JwtUserDetailsService;
import com.assignment.flowerSpot.service.QuoteOfTheDayService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "/application-test.properties")
public class LikeControllerTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@Autowired
	private IUser userRepository;

	@Autowired
	ISighting sightingRepository;

	@Autowired
	IFlower flowerRepository;

	@Autowired
	QuoteOfTheDayService quoteOfTheDayService;

	@Test
	public void authorizedUserShouldBeAbleToLikeSighting() throws Exception {

		User user = new User("admin", "admin", "admin@gmail.com");
		userRepository.save(user);

		Sighting sighting = new Sighting();
		sighting.setId(1L);
		sighting.setLat(12545.2654);
		sighting.setLon(2151559.5151);
		sighting.setImage("IMAGE URL");
		sighting.setFlower(flowerRepository.findById(4L).get());
		sighting.setUser(userRepository.findByUsername("admin"));
		sighting.setQuote(quoteOfTheDayService.getQuoteOfTheDay());
		sightingRepository.save(sighting);

		Long id = sightingRepository.findByFlowerId(4L).getId();

		final UserDetails userDetails = userDetailsService.loadUserByUsername("admin");
		final String token = jwtTokenUtil.generateToken(userDetails);

		mvc.perform(MockMvcRequestBuilders.post("/likeThisSight/" + id).header("Authorization", "Bearer " + token))
				.andExpect(status().isOk());
	}

	@Test
	public void authorizedUserShouldBeAbleToDeleteHisSighting() throws Exception {

		User user = new User("lord", "lord", "lord@gmail.com");
		userRepository.save(user);

		Sighting sighting = new Sighting();
		sighting.setId(2L);
		sighting.setLat(12545.2654);
		sighting.setLon(2151559.5151);
		sighting.setImage("IMAGE URL");
		sighting.setFlower(flowerRepository.findById(5L).get());
		sighting.setUser(userRepository.findByUsername("lord"));
		sighting.setQuote(quoteOfTheDayService.getQuoteOfTheDay());
		sightingRepository.save(sighting);

		Long id = sightingRepository.findByFlowerId(5L).getId();

		final UserDetails userDetails = userDetailsService.loadUserByUsername("lord");
		final String token = jwtTokenUtil.generateToken(userDetails);

		mvc.perform(
				MockMvcRequestBuilders.delete("/deleteLikeOnSight/" + id).header("Authorization", "Bearer " + token))
				.andExpect(status().isOk());

	}

}
