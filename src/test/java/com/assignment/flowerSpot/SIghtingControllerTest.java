package com.assignment.flowerSpot;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.assignment.flowerSpot.model.Sighting;
import com.assignment.flowerSpot.model.User;
import com.assignment.flowerSpot.repository.IFlower;
import com.assignment.flowerSpot.repository.ISighting;
import com.assignment.flowerSpot.repository.IUser;
import com.assignment.flowerSpot.service.QuoteOfTheDayService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "/application-test.properties")
public class SIghtingControllerTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	ISighting sightingRepository;

	@Autowired
	IFlower flowerRepository;

	@Autowired
	IUser userRepository;

	@Autowired
	QuoteOfTheDayService quoteOfTheDayService;

	@Test
	public void shouldReturnListOfSightingsForParticularUser() throws Exception {

		Sighting sighting = new Sighting();
		User user = new User("admin", "admin", "admin@gmail.com");
		userRepository.save(user);

		sighting.setId(1L);
		sighting.setLat(12545.2654);
		sighting.setLon(2151559.5151);
		sighting.setImage("IMAGE URL");
		sighting.setFlower(flowerRepository.findById(4L).get());
		sighting.setUser(userRepository.findByUsername("admin"));
		sighting.setQuote(quoteOfTheDayService.getQuoteOfTheDay());

		sightingRepository.save(sighting);

		MvcResult mvcResult = this.mvc
				.perform(MockMvcRequestBuilders.get("/sighting/byFlowerId/4").accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		System.out.println(mvcResult.getResponse().getContentAsString());

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);

	}

}
